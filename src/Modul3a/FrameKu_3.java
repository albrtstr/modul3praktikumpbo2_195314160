/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul3a;
import MODUL3A.*;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 *
 * @author Me
 */
public class FrameKu_3 extends JFrame {
    public FrameKu_3(){
       this.setSize(300,500);
       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       this.setTitle("Ini Class turunan dari class JFrame");
       this.setVisible(true);
       
       JPanel panel = new JPanel();
       JButton tombol = new JButton();
       JLabel label = new JLabel();
       JTextField textField = new JTextField();
       JCheckBox checkBox = new JCheckBox();
       tombol.setText("Ini Tombol");
       panel.add(tombol);
       this.add(panel);
       this.add(label);
       this.add(textField);
       this.add(checkBox);

}
public static void main(String[]args){
     new FrameKu_3();
   }
}
