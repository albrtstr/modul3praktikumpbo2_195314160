package Modul3b;


import MODUL3B.*;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**Ch14FlowLayoutSample
 *
 * @author Me
 */
    public class Ch14AbsolutePositioning extends JFrame{
        private static final int FRAME_WIDTH=300;
        private static final int FRAME_HEIGHT=200;
        private static final int FRAME_X_ORIGIN=300;
        private static final int FRAME_Y_ORIGIN=100;
        private static final int BUTTON_WIDTH=80;
        private static final int BUTTON_HEIGHT=40;
        private JButton cancelButton;
        private JButton okButton;
        private JTextField textField;
        private JLabel label;
        private JCheckBox checkBox;
        
        public static void main(String []args){
            Ch14AbsolutePositioning frame = new Ch14AbsolutePositioning();
            frame.setVisible(true);
        }
        
        public  Ch14AbsolutePositioning(){
            Container contentPane = getContentPane();
            setSize(FRAME_WIDTH, FRAME_HEIGHT);
            setResizable(true);
            setTitle("Input Data");
            setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
            contentPane.setLayout(null);
            contentPane.setBackground(Color.white);
            
            okButton = new JButton("OK");
            cancelButton = new JButton("Cancel");
            cancelButton.setBounds(300, 400,BUTTON_WIDTH,BUTTON_HEIGHT);
            contentPane.add(cancelButton);
            
            setDefaultCloseOperation(EXIT_ON_CLOSE);
        }
    }
 
        
    
    

